package abduelrahman.rxjavaretrofit_request.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import abduelrahman.rxjavaretrofit_request.Model.Post;
import abduelrahman.rxjavaretrofit_request.R;

public class PostAdapter extends RecyclerView.Adapter<PostAdapter.ViewHolder> {

    private Context context;
    private List<Post> items;

    public PostAdapter(Context context, List<Post> items) {
        this.context = context;
        this.items = items;
    }

    private List<Post> getItems() {
        return items;
    }

    public void setItems(List<Post> items) {
        this.items = items;
        this.notifyDataSetChanged();

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.item_post, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.title.setText(String.valueOf(items.get(position).getTitle()));
        holder.content.setText(String.valueOf(items.get(position).getBody()));
        holder.author.setText(String.valueOf(items.get(position).getUserId()));
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView title, content, author;

        ViewHolder(@NonNull View itemView) {
            super(itemView);

            title = itemView.findViewById(R.id.txt_title);
            content = itemView.findViewById(R.id.txt_content);
            author = itemView.findViewById(R.id.txt_author);
        }

        @Override
        public void onClick(View v) {
            Post m = getItems().get(getAdapterPosition());
        }

    }


}
