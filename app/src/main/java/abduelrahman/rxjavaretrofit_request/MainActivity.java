package abduelrahman.rxjavaretrofit_request;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import java.util.List;

import abduelrahman.rxjavaretrofit_request.Adapter.PostAdapter;
import abduelrahman.rxjavaretrofit_request.Model.Post;
import abduelrahman.rxjavaretrofit_request.Retrofit.Client;
import abduelrahman.rxjavaretrofit_request.Retrofit.MyAPI;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;

public class MainActivity extends AppCompatActivity {

    MyAPI myAPI;
    RecyclerView recyclerView;
    CompositeDisposable compositeDisposable = new CompositeDisposable();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Retrofit retrofit = Client.getInstance();
        myAPI = retrofit.create(MyAPI.class);

        recyclerView = findViewById(R.id.recycler);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        fetchData();
    }

    private void fetchData() {
        Action action = new Action() {
            @Override
            public void run() {
                Toast.makeText(MainActivity.this, "hello", Toast.LENGTH_LONG).show();
            }
        };



        compositeDisposable.add(myAPI.getPosts()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnComplete(action)
                .doOnComplete(new Action() {
                    @Override
                    public void run() {
                        Toast.makeText(MainActivity.this, "hello second", Toast.LENGTH_SHORT).show();
                    }
                }).doOnError(new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) {
                        Toast.makeText(MainActivity.this, throwable.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                    }
                }).subscribe(new Consumer<List<Post>>() {
                    @Override
                    public void accept(List<Post> posts) {

                        if (!posts.isEmpty()) {
                            Toast.makeText(MainActivity.this, "hi again", Toast.LENGTH_SHORT).show();

                            PostAdapter adapter = new PostAdapter(MainActivity.this, posts);
                            recyclerView.setAdapter(adapter);
                        }

                    }
                }));


    }


    @Override
    protected void onStop() {
        compositeDisposable.clear();
        super.onStop();
    }

}
